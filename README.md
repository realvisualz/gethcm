# README #

Human Capital Management is a platform that makes easier managing HR for companies.  

### How do I get set up? ###

* Requirements
* Database configuration
* Setup & Configuration

#####Requirements
* PHP >=5.5.9
* MYSQL 
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
#####Configuring database
After cloning make sure you import database from `sql_scripts/hcm_db.sql` script, but if you already have database imported
and want to update then run `sql_scripts/script.sql` to update or alternatively you can run only one particular script, 
for example want to import only address language fields in language data table.

After that make sure you change credentials in `application/config/database.php` you can either create a new user
or can give your root details.

#### Setup and configuration
 
##### Running platform and technical configuration 
After cloning platform repo in `/xampp/htdocs` or `/var/www/` for ubuntu other http server you're using directory
#####Step 2:
If you don't have ssl configured in http local server, you need to disable it in root app directory in `index.php` 
by commenting this part of code: 
```
if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on")
{
   //Tell the browser to redirect to the HTTPS URL.
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    //Prevent the rest of the script from executing.
    exit;
}
 ``` 
 And remove the `/../` by replacing with `/` in same page.
 
#####Step 3:
Start http server, mysql server and visit [localhost/path/to/platform](http://localhost/path/to/platform),
for example if you've saved in `/xampp/htdocs/hcm` visit [localhost/hcm](http://localhost/hcm).
If Login page is opened then you've configured hcm platform properly.
