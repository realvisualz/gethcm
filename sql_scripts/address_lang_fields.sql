-- Insert new values for address fields in lang table

INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Postal Code', 'Postal Code', NULL, NULL);
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'State/Province', 'State/Province', NULL, NULL);
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Address Line 2', 'Address Line 2', NULL, NULL);