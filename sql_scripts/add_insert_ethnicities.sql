USE hcm_db;

CREATE TABLE sys_ethnicity
(
	id INT auto_increment
		PRIMARY KEY,
	name VARCHAR(255) NOT NULL
)
;

-- Insert ethnic groups in ethnicity table
INSERT INTO `sys_ethnicity` (`id`, `name`) VALUES (NULL, 'White'), (NULL, 'Asian'), (NULL, '	Native Hawaiian'),
(NULL, 'American Indian/Alaskan Native'), (NULL, 'Black or African American'), (NULL, 'Hispanic or Latino'),
(NULL, 'Native Hawaiian or Other Pacific Islander'), (NULL, 'Two or more races');