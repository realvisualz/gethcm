
-- # Add two new fields in employee details

ALTER TABLE hcm_db.sys_employee ADD seniority DATE DEFAULT NULL AFTER doj;
ALTER TABLE hcm_db.sys_employee ADD benefit_seniority DATE DEFAULT NULL AFTER seniority;
