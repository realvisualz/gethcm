USE hcm_db;
-- Insert ethnic/origin column in employees table
ALTER TABLE `sys_employee` ADD `ethnicity` INT NULL DEFAULT NULL AFTER `state_province`;