USE hcm_db;
-- Add address columns
ALTER TABLE `sys_employee` ADD `address` VARCHAR(255) NULL DEFAULT NULL AFTER `remember_token`, ADD `address_line_2` VARCHAR(255) NULL DEFAULT NULL AFTER `address`, ADD `city` VARCHAR(255) NULL DEFAULT NULL AFTER `address_line_2`, ADD `postal_code` VARCHAR(255) NULL DEFAULT NULL AFTER `city`, ADD `coutny` VARCHAR(255) NULL DEFAULT NULL AFTER `postal_code`, ADD `state_province` INT NOT NULL AFTER `coutny`;

-- Insert ethnic/origin row in language
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Ethnic Origin', 'Ethnic Origin', NULL, NULL);

-- Insert new values for address fields in lang table
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Postal Code', 'Postal Code', NULL, NULL);
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'State/Province', 'State/Province', NULL, NULL);
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Address Line 2', 'Address Line 2', NULL, NULL);

-- Change employee code to social security
-- UPDATE sys_language_data SET lan_data = 'Social Security Code', lan_value = 'Social Security Code' WHERE id = 75;

-- Add seniority and benefit seniority fields
ALTER TABLE sys_employee ADD seniority DATE DEFAULT NULL AFTER doj;
ALTER TABLE sys_employee ADD benefit_seniority DATE DEFAULT NULL AFTER seniority;

-- Add language fields for seniority and benefit seniority
INSERT INTO sys_language_data (lan_id, lan_data, lan_value, created_at, updated_at)
VALUES ('1', 'Seniority', 'Seniority', CURRENT_DATE(), CURRENT_DATE());
INSERT INTO hcm_db.sys_language_data (lan_id, lan_data, lan_value, created_at, updated_at)
VALUES ('1', 'Benefit Seniority', 'Benefit Seniority', CURRENT_DATE(), CURRENT_DATE());

-- Add Select origin to language data
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES
 (NULL, '1', 'Select Origin', 'Select Origin', NULL, NULL);

-- Insert i-9 verification in language table
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES
(NULL, '1', 'I-9 verification', 'I-9 verification');

-- Insert new values for tab titles
-- INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Basics', 'Basics', NULL, NULL);
-- INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Dates & Dept', 'Dates & Dept', NULL, NULL);
-- INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Personal', 'Personal', NULL, NULL);

INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Basics', 'Basics', NULL, NULL);
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Dept & Dates', 'Dept & Dates', NULL, NULL);
INSERT INTO `sys_language_data` (`id`, `lan_id`, `lan_data`, `lan_value`, `created_at`, `updated_at`) VALUES (NULL, '1', 'Personal', 'Personal', NULL, NULL);

-- Insert ethnic/origin column in employees table
ALTER TABLE `sys_employee` ADD `ethnicity` INT NULL DEFAULT NULL AFTER `state_province`;

--  Add sys_ethnicity table
CREATE TABLE sys_ethnicity
(
	id INT auto_increment
		PRIMARY KEY,
	name VARCHAR(255) NOT NULL
);
-- Insert ethnic groups in ethnicity table
INSERT INTO `sys_ethnicity` (`id`, `name`) VALUES (NULL, 'White'), (NULL, 'Asian'), (NULL, '	Native Hawaiian'),
(NULL, 'American Indian/Alaskan Native'), (NULL, 'Black or African American'), (NULL, 'Hispanic or Latino'),
(NULL, 'Native Hawaiian or Other Pacific Islander'), (NULL, 'Two or more races');

-- Change employee code to social security
UPDATE sys_language_data SET lan_data = 'SSN', lan_value = 'SSN' WHERE id = 75;

-- Add countries
-- REF: https://gist.github.com/JeremyMorgan/5833666
INSERT into sys_countries values (NULL, 'AL', 'Alabama'),
(NULL, 'AK', 'Alaska'),
(NULL, 'AZ', 'Arizona'),
(NULL, 'AR', 'Arkansas'),
(NULL, 'CA', 'California'),
(NULL, 'CO', 'Colorado'),
(NULL, 'CT', 'Connecticut'),
(NULL, 'DE', 'Delaware'),
(NULL, 'DC', 'District of Columbia'),
(NULL, 'FL', 'Florida'),
(NULL, 'GA', 'Georgia'),
(NULL, 'HI', 'Hawaii'),
(NULL, 'ID', 'Idaho'),
(NULL, 'IL', 'Illinois'),
(NULL, 'IN', 'Indiana'),
(NULL, 'IA', 'Iowa'),
(NULL, 'KS', 'Kansas'),
(NULL, 'KY', 'Kentucky'),
(NULL, 'LA', 'Louisiana'),
(NULL, 'ME', 'Maine'),
(NULL, 'MD', 'Maryland'),
(NULL, 'MA', 'Massachusetts'),
(NULL, 'MI', 'Michigan'),
(NULL, 'MN', 'Minnesota'),
(NULL, 'MS', 'Mississippi'),
(NULL, 'MO', 'Missouri'),
(NULL, 'MT', 'Montana'),
(NULL, 'NE', 'Nebraska'),
(NULL, 'NV', 'Nevada'),
(NULL, 'NH', 'New Hampshire'),
(NULL, 'NJ', 'New Jersey'),
(NULL, 'NM', 'New Mexico'),
(NULL, 'NY', 'New York'),
(NULL, 'NC', 'North Carolina'),
(NULL, 'ND', 'North Dakota'),
(NULL, 'OH', 'Ohio'),
(NULL, 'OK', 'Oklahoma'),
(NULL, 'OR', 'Oregon'),
(NULL, 'PA', 'Pennsylvania'),
(NULL, 'PR', 'Puerto Rico'),
(NULL, 'RI', 'Rhode Island'),
(NULL, 'SC', 'South Carolina'),
(NULL, 'SD', 'South Dakota'),
(NULL, 'TN', 'Tennessee'),
(NULL, 'TX', 'Texas'),
(NULL, 'UT', 'Utah'),
(NULL, 'VT', 'Vermont'),
(NULL, 'VA', 'Virginia'),
(NULL, 'WA', 'Washington'),
(NULL, 'WV', 'West Virginia'),
(NULL, 'WI', 'Wisconsin'),
(NULL, 'WY', 'Wyoming');