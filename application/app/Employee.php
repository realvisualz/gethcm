<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'sys_employee';

    /* designation  Function Start Here */
    public function designation_name()
    {
        return $this->hasOne('App\Designation','id','designation');
    }

    /* department  Function Start Here */
    public function department_name()
    {
        return $this->hasOne('App\Department','id','department');
    }

    /**
     * Employee country
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'state_province');
    }

    /**
     * Employee ethnicity
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ethnic_group()
    {
        return $this->hasOne('App\Ethnicity', 'id', 'ethnicity');
    }


}
