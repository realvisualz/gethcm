@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
@endsection

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Add Employee')}}</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            @include('notification.notify')
            <div class="row">

                <div class="col-md-12">
                    <form id="wizardForm" action="{{url('employees/add-employee-post')}}" method="post">

                        <ul class="nav nav-tabs tabsDisabled" role="tablist">
                            <li class="active">
                                <a href="#basics" class="noFunctiontab" onclick="changeDef(event)">1. {{language_data('Basics')}}</a>
                                <a href="#basics" class="hidden" data-toggle="tab">1. {{language_data('Basics')}}</a>
                            </li>
                            <li>
                                <a href="#dept_dates" class="noFunctiontab" onclick="changeDef(event)">2. {{language_data('Dept & Dates')}}</a>
                                <a href="#dept_dates" class="hidden" data-toggle="tab">2. {{language_data('Dept & Dates')}}</a>
                            </li>
                            <li>
                                <a href="#personal" class="noFunctiontab" onclick="changeDef(event)">3. {{language_data('Personal')}}</a>
                                <a href="#personal" class="hidden" data-toggle="tab">3. {{language_data('Personal')}}</a>
                            </li>
                        </ul>

                        <div class="tab-content panel p-20">

                            <div class="tab-pane active" id="basics">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('First Name')}}</label>
                                            <span class="help">e.g. "Jhon"</span>
                                            <input type="text" class="form-control" name="fname" required>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Last Name')}}</label>
                                            <span class="help">e.g. "Doe"</span>
                                            <input type="text" class="form-control" name="lname" required>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('SSN')}}</label>
                                            <span class="help">e.g. "123-45-6789" ({{language_data('Unique For every User')}})</span>
                                            <input class="form-control" id="ssn" type="zip" name="ssn" title="Expected pattern is ###-##-####" onchange="return checkSSN(event)" onkeypress="return checkSSN(event)" required pattern="\d{3}[\-]\d{2}[\-]\d{4}" minlength="11" maxlength="11">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Username')}}</label>
                                            <span class="help">e.g. "employee" ({{language_data('Unique For every User')}})</span>
                                            <input type="text" class="form-control" name="username" required>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Email')}}</label>
                                            <span class="help">e.g. "youremail@gethcm.com" ({{language_data('Unique For every User')}})</span>
                                            <input type="email" class="form-control" name="email" required>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Password')}}</label>
                                            <input type="password" class="form-control" name="password" required>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Confirm Password')}}</label>
                                            <input type="password" class="form-control" name="rpassword" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('Phone Number')}}</label>
                                            <input type="number" class="form-control" required name="phone">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Alternative Phone')}}</label>
                                            <input type="number" class="form-control" name="phone2">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Tax Template')}}</label>
                                            <select class="selectpicker form-control" name="tax" data-live-search="true">
                                                @foreach($tax as $t)
                                                    <option value="{{$t->id}}">{{$t->tax_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Status')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="status">
                                                <option value="active" selected>{{language_data('Active')}}</option>
                                                <option value="inactive">{{language_data('Inactive')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <ul class="pager">
                                        <li class="next">
                                            <a data-id="dept_dates" data-step="basics" onclick="nextStep(event)" class="btn btn-success">Next <i class="fa fa-angle-right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="tab-pane" id="dept_dates">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="el3">{{language_data('Department')}}</label>
                                            <select class="form-control" name="department" required id="department_id">
                                                <option value="" selected>{{language_data('Select Department')}}</option>
                                                @foreach($department as $d)
                                                    <option value="{{$d->id}}"> {{$d->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="el3">{{language_data('Designation')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="designation" id="designation">
                                                @foreach($designations as $designation)
                                                    <option value="{{$designation->id}}">{{$designation->designation}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('User Role')}}</label>
                                            <select class="selectpicker form-control" data-live-search="true" name="role" required>
                                                <option value="0" selected>{{language_data('Employee')}}</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Date Of Join')}}</label>
                                            <input type="text" class="form-control datePicker" name="doj" required>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Date Of Leave')}}</label>
                                            <input type="text" class="form-control datePicker" name="dol">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Seniority')}}</label>
                                            <input type="text" class="form-control" name="seniority">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Benefit Seniority')}}</label>
                                            <input type="text" class="form-control" name="benefit_seniority">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <ul class="pager">
                                        <li class="previous">
                                            <a data-id="basics" data-step="dept_dates" onclick="prevStep(event)" class="btn btn-success"> <i class="fa fa-angle-left"></i> Previous</a>
                                        </li>
                                        <li class="next">
                                            <a data-id="personal" data-step="dept_dates" onclick="nextStep(event)" class="btn btn-success">Next <i class="fa fa-angle-right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="tab-pane" id="personal">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('Father Name')}}</label>
                                            <input type="text" class="form-control" name="father_name">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Mother Name')}}</label>
                                            <input type="text" class="form-control" name="mother_name">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Date Of Birth')}}</label>
                                            <input type="text" class="form-control datePicker" name="dob" required>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Gender')}}</label>
                                            <select class="form-control" name="gender" required>
                                                <option value="">Select Gender</option>
                                                <option value="Male" >{{language_data('Male')}}</option>
                                                <option value="Female" >{{language_data('Female')}}</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Ethnic Origin')}}</label>
                                            <select class="form-control" name="ethnic_origin" required>
                                                <option value="">Select Ethnic Origin</option>
                                                @foreach($ethnicity as $ethnic_group)
                                                    <option value="{{$ethnic_group->id}}">{{$ethnic_group->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('I-9 verification')}}</label>
                                            <select class="form-control" name="I_9_verification" required>
                                                <option value="" selected>Select I-9 verification</option>
                                                <option value="No">No</option>
                                                <option value="Pending" >Pending</option>
                                                <option value="Yes">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{language_data('Address')}}</label>
                                            <input type="text" class="form-control" name="address">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Address Line 2')}}</label>
                                            <input type="text" class="form-control" name="address_line_2">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('City')}}</label>
                                            <input type="text" class="form-control" name="city">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('State/Province')}}</label>
                                            <select class="selectpicker form-control" name="state_province">
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}">{{$country->country_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Postal Code')}}</label>
                                            <input type="text" class="form-control" name="postal_code">
                                        </div>

                                        <div class="form-group">
                                            <label>{{language_data('Permanent Address')}}</label>
                                            <textarea class="form-control" rows="6" name="per_address"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <ul class="pager">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <li class="previous">
                                            <a data-id="dept_dates" data-step="personal" onclick="prevStep(event)" class="btn btn-success"> <i class="fa fa-angle-left"></i> Previous</a>
                                        </li>
                                        <li class="next">
                                            <a data-id="complete" data-step="personal" onclick="nextStep(event)" class="btn btn-success">Finish <i class="fa fa-check"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <button type="submit" id="subm" class="hidden">Check form</button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>

        function changeDef(e) {
            e.preventDefault();
        }

        function nextStep(e) {
            e.preventDefault();

            /* when submitting wizard form */
            var inputErrors = 0, activeStep = e.target.dataset.step;
            $('#'+activeStep+' input, #'+activeStep+' select').each(
                function(index){
                    var input = $(this);
                    if(!!input.attr('required') && input[0].validity.valueMissing === true) {
                        $("#subm").click();
                        inputErrors += 1;
                    }
                }
            );
            if(inputErrors === 0){
                if(e.target.dataset.id !== 'complete') {
                    $('a[href="#'+e.target.dataset.id+'"]').click()
                } else {
                    $("#subm").click();
                }
            }
        }

        function prevStep(e) {
            e.preventDefault();
            /* when submitting wizard form */
            $('a[href="#'+e.target.dataset.id+'"]').click();
        }

        function checkSSN(e) {
            var charCode = (e.which) ? e.which : event.keyCode;
            console.log(charCode);
            if (charCode != 46 && charCode != 45 && charCode > 31
                && (charCode < 48 || charCode > 57)){
                return false;
            } else {
                var x = e.target.value;
                e.target.value = e.target.value.replace(/(\d{3})(\d{2})(\d{4})/, "$1-$2-$3")
                return true;
            }
        }

        $(document).ready(function () {

            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/employee/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });
        });
    </script>

@endsection
